################################################################################
########
######## KeynesCalc V0.01: Started 9/6/21, by Ulysses
########
################################################################################

import tkinter as tk
from tkcalendar import DateEntry
import time

elements = []


funcList = ["Elasticity of Demand", "Elasticity of Supply", "Calculate GDP"]

def elasDemand(QD1, QD2, P1, P2):
    changeQD = QD2 - QD1
    changeQDBottom = (QD2 + QD1) / 2

    changeP = P2 - P1
    changePBottom = (P2 + P1) / 2

    finalQD = changeQD / changeQDBottom

    finalP = changeP / changePBottom

    answer = finalQD / finalP

    newAnswer = abs(round(answer, 2))

    answerLabel.config(text= "Answer: " + str(newAnswer))

def elasSupply(QS1, P1):

    answer = QS1 / P1

    newAnswer = round(answer, 5)

    answerLabel.config(text= "Answer: " + str(newAnswer))

def calcGDP(C, I, G, NE):
    Y = C + I + G + NE

    answerLabel.config(text= "Answer: " + str(Y))

def optionSelection(self):
    selectedFunc = funcVar.get()

    def convertValuesElasDemand():
        elasDemand(float(Q1Enter.get()), float(Q2Enter.get()), float(P1Enter.get()), float(P2Enter.get()))
    
    def convertValuesElasSupply():
        elasSupply(float(Q1Enter.get()), float(P1Enter.get()))
    
    def convertValuesGDP():
        calcGDP(float(CEnter.get()), float(IEnter.get()), float(GEnter.get()), float(NEEnter.get()))

    for e in elements:
        e.pack_forget() # Deletes all previously added entry tables

    if selectedFunc == "Elasticity of Demand":
        Q1Label = tk.Label(selectionWindow, text = "Quantity Demanded 1:")
        Q1Label.pack()
        Q1Enter = tk.Entry(selectionWindow)
        Q1Enter.pack()

        Q2Label = tk.Label(selectionWindow, text = "Quantity Demanded 2:")
        Q2Label.pack()
        Q2Enter = tk.Entry(selectionWindow)
        Q2Enter.pack()

        P1Label = tk.Label(selectionWindow, text = "Price 1:")
        P1Label.pack()
        P1Enter = tk.Entry(selectionWindow)
        P1Enter.pack()

        P2Label = tk.Label(selectionWindow, text = "Price 2:")
        P2Label.pack()
        P2Enter = tk.Entry(selectionWindow)
        P2Enter.pack()

        goBtn = tk.Button(selectionWindow, text = 'Calculate', bd = '5', command = convertValuesElasDemand) ### Button runs the selection command when pressed
        goBtn.pack(side = 'bottom')  

        elements.extend([Q1Label, Q1Enter, Q2Label, Q2Enter, P1Label, P1Enter, P2Label, P2Enter, goBtn]) # Adds the entry tables to the array
    
    if selectedFunc == "Elasticity of Supply":
        Q1Label = tk.Label(selectionWindow, text = "Percent Change of Quantity Supplied:")
        Q1Label.pack()
        Q1Enter = tk.Entry(selectionWindow)
        Q1Enter.pack()

        P1Label = tk.Label(selectionWindow, text = "Percent Change of Price:")
        P1Label.pack()
        P1Enter = tk.Entry(selectionWindow)
        P1Enter.pack()

        goBtn = tk.Button(selectionWindow, text = 'Calculate', bd = '5', command = convertValuesElasSupply) ### Button runs the selection command when pressed
        goBtn.pack(side = 'bottom') 

        elements.extend([Q1Label, Q1Enter, P1Label, P1Enter, goBtn]) # Adds the entry tables to the array
    
    if selectedFunc == "Calculate GDP":
        CLabel = tk.Label(selectionWindow, text = "Consumption:")
        CLabel.pack()
        CEnter = tk.Entry(selectionWindow)
        CEnter.pack()

        ILabel = tk.Label(selectionWindow, text = "Investment:")
        ILabel.pack()
        IEnter = tk.Entry(selectionWindow)
        IEnter.pack()

        GLabel = tk.Label(selectionWindow, text = "Government Spending:")
        GLabel.pack()
        GEnter = tk.Entry(selectionWindow)
        GEnter.pack()

        NELabel = tk.Label(selectionWindow, text = "Net Exports:")
        NELabel.pack()
        NEEnter = tk.Entry(selectionWindow)
        NEEnter.pack()

        goBtn = tk.Button(selectionWindow, text = 'Calculate', bd = '5', command = convertValuesGDP) ### Button runs the selection command when pressed
        goBtn.pack(side = 'bottom') 

        elements.extend([CLabel, CEnter, ILabel, IEnter, GLabel, GEnter, NELabel, NEEnter, goBtn]) # Adds the entry tables to the array



############ Do GDP calcs tonight.

########################################
#### TKINTER WINDOW
########################################


selectionWindow = tk.Tk()
selectionWindow.geometry("500x400")
title = tk.Label(text= "Welcome to the Keynes Calculator!")
title.pack()
credits = tk.Label(text = "Made by Wooden Horse Productions")
credits.pack(side = 'bottom')

funcVar = tk.StringVar(selectionWindow)
funcVar.set("What do you want to calculate:") ### This is the default option, basically what shows up on the list before you've selected something
funcListMenu = tk.OptionMenu(selectionWindow, funcVar, *funcList, command = optionSelection)
funcListMenu.pack()

answerLabel = tk.Label(text='Answer:')
answerLabel.pack(side = 'bottom')

selectionWindow.mainloop()
